/**
 * 
 */

function valida(){
	
	var x = document.forms["myForm"]["origem"].value;
	var dp = document.forms["myForm"]["dataPedido"].value;
	var de = document.forms["myForm"]["dataEvento"].value;
	var c = document.forms["myForm"]["cerimonial"].value;
	var n = document.forms["myForm"]["numero"].value;
	var b = document.forms["myForm"]["bairro"].value;
	var cidade = document.forms["myForm"]["cidade"].value;
	var estado = document.forms["myForm"]["estado"].value;
	var cep = document.forms["myForm"]["cep"].value;
	var rua = document.forms["myForm"]["rua"].value;
	
	if (x == null || x == "") {
        window.alert("Campo 'Origem' é obrigatório!");
        return false;
    } else if (dp == null || dp == "") {
        window.alert("Campo 'Data do Pedido' é obrigatório!");
        return false;
    } else if (de == null || de == "") {
        window.alert("Campo 'Data do Evento' é obrigatório!");
        return false;
    }else if (c == null || c == "") {
        window.alert("Campo 'Cerimonial' é obrigatório!");
        return false;
    }else if (n == null || n == "") {
        window.alert("Campo 'Número' é obrigatório!");
        return false;
    }else if (b == null || b == "") {
        window.alert("Campo 'Bairro' é obrigatório!");
        return false;
    }else if (cidade == null || cidade == "") {
        window.alert("Campo 'Cidade' é obrigatório!");
        return false;
    }else if (estado == null || estado == "") {
        window.alert("Campo 'Estado' é obrigatório!");
        return false;
    }else if (cep == null || cep == "") {
        window.alert("Campo 'CEP' é obrigatório!");
        return false;
    }else if (rua == null || rua == "") {
        window.alert("Campo 'Rua' é obrigatório!");
        return false;
    }else{
    	return true;
    }
}

function myFunction(){
	window.alert("OK");
}