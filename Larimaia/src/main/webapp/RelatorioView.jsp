<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<link rel="stylesheet" type="text/css" href="layout.css">
<title>Insert title here</title>
</head>
<body>
	<%
	//allow access only if session exists
	if(session.getAttribute("user") == null){
	    response.sendRedirect("Login.html");
	}
	String userName = null;
	String sessionID = null;
	Cookie[] cookies = request.getCookies();
	if(cookies !=null){
		for(Cookie cookie : cookies){
		    if(cookie.getName().equals("user")) userName = cookie.getValue();
		}
	}
	%>
	
	<div class="menu">
		<ul class="menu-list">
				<li>
					<a href="#">File</a>
					<ul class="sub-menu">
						<li><a href="${pageContext.request.contextPath}/LogoutServlet">Sair</a></li>
						<li><a href="/Larimaia/RelatorioView.jsp">Filtrar Pedidos</a></li>
					</ul>
				</li>
				<li>
					<a href="#">Cadastro</a>
				       <ul class="sub-menu">
				        <li><a href="/Larimaia/CadClienteView.jsp">Clientes</a></li>
				        <li><a href="/Larimaia/CadProdutoView.jsp">Produtos</a></li>
				        <li><a href="/Larimaia/CadPedidoView.jsp">Pedidos</a></li>
				      </ul>
				</li>
				<li>
					<a href="#">Listas</a>
				       <ul class="sub-menu">
				        <li><a href="/Larimaia/ListaClientes.jsp">Clientes</a></li>
				        <li><a href="/Larimaia/ListaProdutos.jsp">Produtos</a></li>
				        <li><a href="/Larimaia/ListaPedidos.jsp">Pedidos</a></li>
				      </ul>
				</li>
		</ul>
	</div>
	
	<form class="formulario" action="RelatorioController" name="myForm" method="POST">
	
		<h1 align="center">Filtro de Pedidos</h1>
	
		<div class="form1">
			<legend class="news">Por Cliente</legend>
			
			<jsp:useBean id="dao" class="br.com.larimaia.dao.ClienteDAO" />
			<label class="rotulo"> Cliente: </label> 
			<select class="texto" name="cliente" id="cliente">
				<c:forEach var="cliente" items="${dao.lista}">
					<option value="${cliente.idCliente}">${cliente}</option>
				</c:forEach>
			</select>
			<input class="botao" type="submit" name="acao" value="Buscar por Cliente">
			
		</div>
		
		<div class="form4">
			<legend class="news">Por Data Contrato</legend>
			<label class="rotulo"> Data Inicial: </label>
			<input class="texto" type="date" name="dataInicial" id="dataInicial" />
			<br>
			<label class="rotulo"> Data Final: </label>
			<input class="texto" type="date" name="dataFinal" id="dataFinal" />
			<input class="botao" type="submit" name="acao" value="Buscar por Contrato" >
		</div>
		
		<div class="form5">
			<legend class="news">Por Data Evento</legend>
			<label class="rotulo"> Data Inicial: </label>
			<input class="texto" type="date" name="dataInicial1" id="dataInicial" />
			<br>
			<label class="rotulo"> Data Final: </label>
			<input class="texto" type="date" name="dataFinal1" id="dataFinal" />
			<input class="botao" type="submit" name="acao" value="Buscar por Data Evento" >
		</div>
		
		<div class="form2">
			<table border="1">
				<thead>
					<th>ID</th>
					<th>Cliente</th>
					<th>Data Contrato</th>
					<th>Data Evento</th>
					<th>Evento</th>
					<th>Valor</th>
					
				</thead>
				<tbody>
					<c:set var="sum" value="${0}"/>
					<c:forEach items="${requestScope.peds}" var="pedido">
						<tr>
							<td><c:out value="${pedido.idPedido}" />
							<td><c:out value="${pedido.cliente}" />
							<td><c:out value="${pedido.dataPedido}" />
							<td><c:out value="${pedido.dataHoraEvento}" />
							<td><c:out value="${pedido.tipoEvento}" />
							<td><c:out value="${pedido.valor}" />
						</tr>
						<c:set var="sum" value="${sum + pedido.valor}"/>
					</c:forEach>
					
				</tbody>
				<tfoot>
					<td>Valor Total</td>
					<td colspan=5 align="right"><c:out value="${sum}" />
				</tfoot>
			</table>
		</div>
			
	</form>
</body>
</html>