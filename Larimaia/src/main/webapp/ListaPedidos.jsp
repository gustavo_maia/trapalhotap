<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<link rel="stylesheet" type="text/css" href="layout.css">
<title>Lista de Pedidos</title>
</head>
<body>

	<%
		//allow access only if session exists
		if(session.getAttribute("user") == null){
		    response.sendRedirect("Login.html");
		}
		String userName = null;
		String sessionID = null;
		Cookie[] cookies = request.getCookies();
		if(cookies !=null){
			for(Cookie cookie : cookies){
			    if(cookie.getName().equals("user")) userName = cookie.getValue();
			}
		}
	%>
	
	<div class="menu">
		<ul class="menu-list">
				<li>
					<a href="#">File</a>
					<ul class="sub-menu">
						<li><a href="${pageContext.request.contextPath}/LogoutServlet">Sair</a></li>
						<li><a href="/Larimaia/RelatorioView.jsp">Filtrar Pedidos</a></li>
					</ul>
				</li>
				<li>
					<a href="#">Cadastro</a>
				       <ul class="sub-menu">
				        <li><a href="/Larimaia/CadClienteView.jsp">Clientes</a></li>
				        <li><a href="/Larimaia/CadProdutoView.jsp">Produtos</a></li>
				        <li><a href="/Larimaia/CadPedidoView.jsp">Pedidos</a></li>
				      </ul>
				</li>
				<li>
					<a href="#">Listas</a>
				       <ul class="sub-menu">
				        <li><a href="/Larimaia/ListaClientes.jsp">Clientes</a></li>
				        <li><a href="/Larimaia/ListaProdutos.jsp">Produtos</a></li>
				        <li><a href="/Larimaia/ListaPedidos.jsp">Pedidos</a></li>
				      </ul>
				</li>
		</ul>
	</div>
	
	<form class="formulario">
	
	<h1 align="center"> Lista de Pedidos</h1>
	
	<jsp:useBean id="dao" class="br.com.larimaia.dao.PedidoDAO"/>
	<table border="1">
		<thead>
		      <th>ID</th>
		      <th>Cliente</th>
		      <th>Valor</th>
		      <th colspan=2>A��o</th>
		</thead>
        <tbody>
	    	<c:forEach items="${dao.lista}" var="pedido">
		        <tr>
		          <td><c:out value="${pedido.idPedido}" /></td>
		          <td><c:out value="${pedido.cliente}" /></td>
		          <td><c:out value="${pedido.valor}" /></td>
		          <td><a href="PedidoController?acao=edit&userId=<c:out value="${pedido.idPedido}"/>">Editar</a></td>
                  <td><a href="PedidoController?acao=delete&userId=<c:out value="${pedido.idPedido}"/>">Excluir</a></td>
		        </tr>
	    	</c:forEach>
	    </tbody>
    </table>
	<p><a href="PedidoController?acao=cadastrar">Realizar Pedido</a></p>
	
	</form>
</body>
</html>