<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<link rel="stylesheet" type="text/css" href="layout.css">
<script src="<c:url value="script.js" />"></script>
<title>Cadastro de Pedidos</title>
</head>
<body>

	<%
	//allow access only if session exists
	if(session.getAttribute("user") == null){
	    response.sendRedirect("Login.html");
	}
	String userName = null;
	String sessionID = null;
	Cookie[] cookies = request.getCookies();
	if(cookies !=null){
		for(Cookie cookie : cookies){
		    if(cookie.getName().equals("user")) userName = cookie.getValue();
		}
	}
	%>

	<div class="menu">
		<ul class="menu-list">
				<li>
					<a href="#">File</a>
					<ul class="sub-menu">
						<li><a href="${pageContext.request.contextPath}/LogoutServlet">Sair</a></li>
						<li><a href="/Larimaia/RelatorioView.jsp">Filtrar Pedidos</a></li>
					</ul>
				</li>
				<li>
					<a href="#">Cadastro</a>
				       <ul class="sub-menu">
				        <li><a href="/Larimaia/CadClienteView.jsp">Clientes</a></li>
				        <li><a href="/Larimaia/CadProdutoView.jsp">Produtos</a></li>
				        <li><a href="/Larimaia/CadPedidoView.jsp">Pedidos</a></li>
				      </ul>
				</li>
				<li>
					<a href="#">Listas</a>
				       <ul class="sub-menu">
				        <li><a href="/Larimaia/ListaClientes.jsp">Clientes</a></li>
				        <li><a href="/Larimaia/ListaProdutos.jsp">Produtos</a></li>
				        <li><a href="/Larimaia/ListaPedidos.jsp">Pedidos</a></li>
				      </ul>
				</li>
		</ul>
	</div>

	<form class="formulario" action="PedidoController" name="myForm" method="POST" onsubmit="javascript:return valida()">

		<h1 align="center">Cadastro de Pedidos</h1>

		<div class="form1">

			<legend class="news">Informa��es</legend>

			<br>

			<jsp:useBean id="dao" class="br.com.larimaia.dao.ClienteDAO" />
			<label class="rotulo"> Cliente: </label> <select class="texto"
				name="cliente" id="cliente">
				<c:forEach var="cliente" items="${dao.lista}">
					<option value="${cliente.idCliente}">${cliente}</option>
				</c:forEach>
			</select> <br> <label class="rotulo"> Data Pedido: </label> <input
				class="texto" type="date" name="dataPedido" id="dataPedido" /> <br> <label
				class="rotulo"> Data Evento: </label> <input class="texto"
				type="date" name="dataEvento" id="dataEvento" /> <br> 
				<label class="rotulo">Origem do Pedido: </label> 
				<input class="texto" type="text" name="origem" id="origem">

			<br> <label class="rotulo"> Cerimonial: </label> <input
				class="texto" type="text" name="cerimonial" id="cerimonial"> <br> <label
				class="rotulo"> Indica��o: </label> <input class="texto" type="text"
				name="indicacao"> <br> <label class="rotulo">
				Observa��o: </label> <input class="texto" type="text" name="observacao">

			<br>

			<jsp:useBean id="tipoEventoDao"
				class="br.com.larimaia.dao.TipoEventoDAO" />
			<label class="rotulo"> Tipo de Evento: </label> <select class="texto"
				name="tipoEvento">
				<c:forEach var="tipoEvento" items="${tipoEventoDao.lista}">
					<option value="${tipoEvento.idTipoEvento}">${tipoEvento}</option>
				</c:forEach>
			</select>

		</div>

		<br>

		<div class="form2">

			<legend class="news">Endere�o de Entrega</legend>
			<br> 
			<label class="rotulo"> Estado: </label> 
			<input class="texto" type="text" name="estado" id="estado">
			 <br> 
			<label class="rotulo"> Cidade: </label>
			<input class="texto" type="text" name="cidade" id="cidade">
			 <br> 
			<label class="rotulo"> CEP: </label> 
			<input class="texto" type="text" name="cep" id="cep">
			 <br> 
			<label class="rotulo"> Bairro: </label>
			<input class="texto" type="text" name="bairro" id="bairro">
			 <br> 
			<label class="rotulo"> Rua: </label>
			<input class="texto" type="text" name="rua" id="rua">
			 <br> 
			<label class="rotulo"> N�mero: </label>
			<input class="texto" type="text" name="numero" id="numero">

		</div>

		<br>

		<div class="form3">

			<legend class="news">Carrinho</legend>

			<br>

			<jsp:useBean id="produtoDao" class="br.com.larimaia.dao.ProdutoDAO" />
			<label class="rotulo"> Produto: </label> <select class="texto"
				name="produto">
				<c:forEach var="produto" items="${produtoDao.lista}">
					<option value="${produto.idProduto}">${produto}</option>
				</c:forEach>
			</select> <br> <label class="rotulo"> Quantidade: </label> <input
				class="texto" type="text" name="quantidade"> <br> <label
				class="rotulo"> Produto: </label> <select class="texto"
				name="produto2">
				<c:forEach var="produto" items="${produtoDao.lista}">
					<option value="${produto.idProduto}">${produto}</option>
				</c:forEach>
			</select> <br> <label class="rotulo"> Quantidade: </label> <input
				class="texto" type="text" name="quantidade2"> <br> <label
				class="rotulo"> Produto: </label> <select class="texto"
				name="produto3">
				<c:forEach var="produto" items="${produtoDao.lista}">
					<option value="${produto.idProduto}">${produto}</option>
				</c:forEach>
			</select> <br> <label class="rotulo"> Quantidade: </label> <input
				class="texto" type="text" name="quantidade3"> <br>

			<label class="rotulo">Tabela de Pre�os</label>
			<br>

			<table border="1">
				<thead>
					<th>Produto</th>
					<th>Valor Unit�rio</th>
				</thead>
				<tbody>
					<c:forEach items="${produtoDao.lista}" var="produto">
						<tr>
							<td><c:out value="${produto}" />
							<td><c:out value="${produto.valor}" />
						</tr>
					</c:forEach>
				</tbody>
			</table>

		</div>

		<div class="botoes" align="center">

			<input class="botao" type="submit" name="acao" value="Salvar">
			<input class="botao" type="reset" value="Cancelar"> <input
				class="botao" type="button" name="acao" value="Listar" onclick="location.href='PedidoController?acao=listar'">

		</div>
	</form>
</body>
</html>