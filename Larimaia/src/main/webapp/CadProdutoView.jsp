<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<link rel="stylesheet" type="text/css" href="layout.css">
<script src="<c:url value="ClienteScript.js" />"></script>
<title>Cadastro de Produtos</title>
</head>
<body>
	
	<%
	//allow access only if session exists
	if(session.getAttribute("user") == null){
	    response.sendRedirect("Login.html");
	}
	String userName = null;
	String sessionID = null;
	Cookie[] cookies = request.getCookies();
	if(cookies !=null){
		for(Cookie cookie : cookies){
		    if(cookie.getName().equals("user")) userName = cookie.getValue();
		}
	}
	%>
	
	<div class="menu">
		<ul class="menu-list">
				<li>
					<a href="#">File</a>
					<ul class="sub-menu">
						<li><a href="${pageContext.request.contextPath}/LogoutServlet">Sair</a></li>
						<li><a href="/Larimaia/RelatorioView.jsp">Filtrar Pedidos</a></li>
					</ul>
				</li>
				<li>
					<a href="#">Cadastro</a>
				       <ul class="sub-menu">
				        <li><a href="/Larimaia/CadClienteView.jsp">Clientes</a></li>
				        <li><a href="/Larimaia/CadProdutoView.jsp">Produtos</a></li>
				        <li><a href="/Larimaia/CadPedidoView.jsp">Pedidos</a></li>
				      </ul>
				</li>
				<li>
					<a href="#">Listas</a>
				       <ul class="sub-menu">
				        <li><a href="/Larimaia/ListaClientes.jsp">Clientes</a></li>
				        <li><a href="/Larimaia/ListaProdutos.jsp">Produtos</a></li>
				        <li><a href="/Larimaia/ListaPedidos.jsp">Pedidos</a></li>
				      </ul>
				</li>
		</ul>
	</div>
	
	<form class="formulario" action="ProdutoController" name="myForm" method="POST" onsubmit="javascript:return valida()">
	
		<h1 align="center">Cadastro de Produtos</h1>
		
		<div class="form2">

			<legend class="news">Informa��es</legend>

			 <br>
			 <input type="hidden" name="id" value="${requestScope.pro.idProduto}">
			<label class="rotulo"> Descri��o: </label> 
			<input class="texto" type="text" name="desc" id="desc" value="${requestScope.pro.descricao}">
			 <br>
			 <label class="rotulo"> Valor Unit�rio: </label> 
			<input class="texto" type="text" name="valor" id="valor" value="${requestScope.pro.valor}">
			<br>
		</div>
		
		<div class="botoes2" align="center">

			<input class="botao" type="submit" name="acao" value="Salvar">
			<input class="botao" type="reset" value="Cancelar"> <input
				class="botao" type="button" name="acao" value="Listar" onclick="location.href='ProdutoController?acao=listar'">

		</div>
		
	</form>
	
</body>
</html>