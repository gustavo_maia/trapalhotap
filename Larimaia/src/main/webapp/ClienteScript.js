/**
 * 
 */

function valida(){
	
	var nome = document.forms["myForm"]["nome"].value;
	var fone = document.forms["myForm"]["telefone"].value;
	var mail = document.forms["myForm"]["mail"].value;
	
	if (nome == null || nome == "") {
        window.alert("Campo 'Nome' é obrigatório!");
        return false;
    } else if (fone == null || fone == "") {
        window.alert("Campo 'Telefone' é obrigatório!");
        return false;
    } else if (mail == null || mail == "") {
        window.alert("Campo 'E-mail' é obrigatório!");
        return false;
    }else{
    	return true;
    }
}