package br.com.larimaia.service;

import br.com.larimaia.dao.PedidoDAO;
import br.com.larimaia.model.Pedido;
import br.com.larimaia.util.ServiceException;

public class PedidoService {
	private PedidoDAO pedidoDAO;

	public PedidoService() {
		pedidoDAO = new PedidoDAO();
	}

	public void salvar(Pedido pedido) throws ServiceException {
		pedidoDAO.salvar(pedido);
	}

	public void excluir(Integer id) {
		pedidoDAO.excluir(id);
	}
}
