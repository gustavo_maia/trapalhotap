package br.com.larimaia.controller;

import java.io.IOException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import br.com.larimaia.dao.ClienteDAO;
import br.com.larimaia.dao.PedidoDAO;
import br.com.larimaia.model.Pedido;

@WebServlet({ "/RelatorioServlet", "/RelatorioController",
"/RelatorioController.do" })
public class RelatorioController extends HttpServlet {

	private static final long serialVersionUID = 1L;
	PedidoDAO pedidoDao = new PedidoDAO();
	List<Pedido> listaPedidos = new ArrayList<>();
	
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		
		String botao = request.getParameter("acao");
		
		if(botao.equals("Buscar por Cliente")){
			String cli = request.getParameter("cliente");
			listaPedidos = pedidoDao.getPorCliente(Integer.parseInt(cli));
			request.setAttribute("peds", listaPedidos);			
		}else if(botao.equals("Buscar por Contrato")){
			String inicial = request.getParameter("dataInicial");
			String fim = request.getParameter("dataFinal");
			Date dataInicial = new Date();
			Date dataFinal = new Date();
			DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");  
			try {
				dataFinal = (java.util.Date)formatter.parse(fim);
				dataInicial = (java.util.Date)formatter.parse(inicial);
			} catch (ParseException e) {
				e.printStackTrace();
			}
			if(dataFinal.compareTo(dataInicial) < 0){
				response.getWriter().print("<script> window.alert('Datas Inválidas');</script>");
			}else{
				java.sql.Date sqlDate = new java.sql.Date(dataInicial.getTime());
				java.sql.Date sqlDate2 = new java.sql.Date(dataFinal.getTime());
				listaPedidos = pedidoDao.getPorContrato(sqlDate, sqlDate2);
				request.setAttribute("peds", listaPedidos);
			}
			
		}else if(botao.equals("Buscar por Data Evento")){
			String inicial = request.getParameter("dataInicial1");
			String fim = request.getParameter("dataFinal1");
			Date dataInicial = new Date();
			Date dataFinal = new Date();
			DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");  
			if(inicial.equals("") || fim.equals("")){
				response.getWriter().print("<script> window.alert('Datas Inválidas');</script>");
			}else{
				try {
					dataFinal = (java.util.Date)formatter.parse(fim);
					dataInicial = (java.util.Date)formatter.parse(inicial);
				} catch (ParseException e) {
					e.printStackTrace();
				}
				if(dataFinal.compareTo(dataInicial) < 0){
					response.getWriter().print("<script> window.alert('Datas Inválidas');</script>");
				}else{
					java.sql.Date sqlDate = new java.sql.Date(dataInicial.getTime());
					java.sql.Date sqlDate2 = new java.sql.Date(dataFinal.getTime());
					listaPedidos = pedidoDao.getPorDataEvento(sqlDate, sqlDate2);
					request.setAttribute("peds", listaPedidos);
				}
			}
			
		}
		
		// Levar para o JSP
		RequestDispatcher view = request.getRequestDispatcher("RelatorioView.jsp");
		view.forward(request, response);
		
	}
	
}
