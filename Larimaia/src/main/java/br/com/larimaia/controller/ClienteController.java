package br.com.larimaia.controller;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import br.com.larimaia.dao.ClienteDAO;
import br.com.larimaia.dao.ItemPedidoDAO;
import br.com.larimaia.dao.PedidoDAO;
import br.com.larimaia.model.Cliente;

@WebServlet({ "/ClienteServlet", "/ClienteController",
"/ClienteController.do" })
public class ClienteController extends HttpServlet {

	private ClienteDAO dao = new ClienteDAO();
	private Cliente cliente;
	
	private static final long serialVersionUID = 1L;
	private static String INSERT_OR_EDIT = "/CadClienteView.jsp";
    private static String LIST_USER = "/ListaClientes.jsp";
	
	protected void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		
		String forward="";
		String action = req.getParameter("acao");
		if (action.equalsIgnoreCase("delete")){
			ClienteDAO dao = new ClienteDAO();
			String userId = req.getParameter("userId");
			dao.excluir(Integer.parseInt(userId));
            resp.getWriter().print("<script> window.alert('Excluido com Sucesso!');</script>");
            forward = LIST_USER;   
        }else if(action.equalsIgnoreCase("listar")){
			forward = LIST_USER;
		}else if(action.equalsIgnoreCase("cadastrar")){
			forward = INSERT_OR_EDIT;
		}else if(action.equalsIgnoreCase("edit")){
			forward = INSERT_OR_EDIT;
			String id = req.getParameter("userId");
			Cliente clienteBuscado = dao.buscarPorId(Integer
					.parseInt(id));

			req.setAttribute("cli", clienteBuscado);
		}
		
		// Levar para o JSP
		RequestDispatcher view = req.getRequestDispatcher(forward);
		view.forward(req, resp);
		
	}
	
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		response.setContentType("text/html");
		
		String acao = request.getParameter("acao");
		
		if(acao.equals("Salvar")){
			cliente = new Cliente();
			
			String nome = request.getParameter("nome");
			cliente.setNome(nome);
			
			String email = request.getParameter("mail");
			cliente.setEmail(email);
			
			String telefone = request.getParameter("telefone");
			cliente.setTelefone(telefone);
			
			String id = request.getParameter("id");
			if (id != null && id != "" && id!="0") {
				cliente.setIdCliente(Integer.parseInt(id));
			}
			
			ClienteDAO dao = new ClienteDAO();
			dao.salvar(cliente);
			
			response.getWriter().print("<script> window.alert('Salvo com Sucesso!'); location.href='ClienteController?acao=listar'</script>");
			
		}else if(acao.equals("Listar")){
			doGet(request, response);
		}
		
	}
	
}
