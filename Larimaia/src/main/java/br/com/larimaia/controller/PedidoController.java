package br.com.larimaia.controller;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import br.com.larimaia.dao.EnderecoDAO;
import br.com.larimaia.dao.ItemPedidoDAO;
import br.com.larimaia.dao.PedidoDAO;
import br.com.larimaia.dao.ProdutoDAO;
import br.com.larimaia.model.Cliente;
import br.com.larimaia.model.Endereco;
import br.com.larimaia.model.ItemPedido;
import br.com.larimaia.model.Pedido;
import br.com.larimaia.model.Produto;
import br.com.larimaia.model.TipoEvento;

@WebServlet({ "/PedidoServlet", "/PedidoController",
		"/PedidoController.do" })
public class PedidoController extends HttpServlet {
	
	private static final long serialVersionUID = 1L;
	private static String INSERT_OR_EDIT = "/CadPedidoView.jsp";
    private static String LIST_USER = "/ListaPedidos.jsp";
	Pedido pedido;
	Endereco endereco;
	PedidoDAO pedidoDao;
	EnderecoDAO enderecoDao;
	ItemPedidoDAO itemPedidoDAO;
	
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		
		String forward="";
		String action = req.getParameter("acao");
		if (action.equalsIgnoreCase("delete")){
            String userId = req.getParameter("userId");
            pedidoDao = new PedidoDAO();
            itemPedidoDAO = new ItemPedidoDAO();
            itemPedidoDAO.excluir(Integer.parseInt(userId));
            pedidoDao.excluir(Integer.parseInt(userId));
            resp.getWriter().print("<script> window.alert('Excluido com Sucesso!');</script>");
            forward = LIST_USER;   
        }else if(action.equalsIgnoreCase("listar")){
			forward = LIST_USER;
		}else if(action.equalsIgnoreCase("cadastrar")){
			forward = INSERT_OR_EDIT;
		}
		
		// Levar para o JSP
		RequestDispatcher view = req.getRequestDispatcher(forward);
		view.forward(req, resp);
		
		
	}

	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		response.setContentType("text/html");
		
		String acao = request.getParameter("acao");
		
		if(acao.equals("Salvar")){
			
			pedido = new Pedido();
            pedido.setCerimonial(request.getParameter("cerimonial"));
            pedido.setOrigemPedido(request.getParameter("origem"));
			
            String cliente = request.getParameter("cliente");
			pedido.setIdCliente(Integer.parseInt(cliente));
			
			Date dataEvento = new Date();
			try {
				dataEvento = new SimpleDateFormat("dd-MM-yyyy").parse(request.getParameter("dataEvento"));
			} catch (ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
            pedido.setDataHoraEvento(dataEvento);
            
            Date dataPedido = new Date();
			try {
				dataPedido = new SimpleDateFormat("dd-MM-yyyy").parse(request.getParameter("dataPedido"));
			} catch (ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
            pedido.setDataPedido(dataPedido);
            
            pedido.setIndicacao(request.getParameter("indicacao"));
//            pedido.setListaItemPedido(request.getParameter("origem"));
            pedido.setObservacao(request.getParameter("observacao"));
            
            String tipoEvento = request.getParameter("tipoEvento");
			pedido.setIdTipoEvento(Integer.parseInt(tipoEvento));
			
			String idP = request.getParameter("produto");
			Integer id1 = Integer.parseInt(idP);
			String qtd = request.getParameter("quantidade");
			Double qtd2 = Double.parseDouble(qtd);
			
			ItemPedido ip1 = new ItemPedido();
			ip1.setIdProduto(id1);
			ip1.setQuantidade(Integer.parseInt(qtd));
			ProdutoDAO proDao = new ProdutoDAO();
			Produto p = proDao.buscarPorId(id1);
			ip1.setValor(p.getValor() * qtd2);
			
			//===================================================
			
			String idPII = request.getParameter("produto2");
			Integer idII1 = Integer.parseInt(idPII);
			String qtdII = request.getParameter("quantidade2");
			Double qtdII2 = Double.parseDouble(qtdII);
			
			ItemPedido ip2 = new ItemPedido();
			ip2.setIdProduto(idII1);
			ip2.setQuantidade(Integer.parseInt(qtdII));
			Produto p2 = proDao.buscarPorId(idII1);
			ip2.setValor(p2.getValor() * qtdII2);
			
			//=====================================================
			
			String pro3 = request.getParameter("produto3");
			Integer idP3 = Integer.parseInt(pro3);
			String qtdP3 = request.getParameter("quantidade3");
			Double q3 = Double.parseDouble(qtdP3);
			
			ItemPedido ip3 = new ItemPedido();
			ip3.setIdProduto(idP3);
			ip3.setQuantidade(Integer.parseInt(qtdP3));
			Produto p3 = proDao.buscarPorId(idP3);
			ip3.setValor(p3.getValor() * q3);
			
			List<ItemPedido> listaIP = new ArrayList<>();
			listaIP.add(ip1);
			listaIP.add(ip2);
			listaIP.add(ip3);
			
			//Endere�o
			endereco = new Endereco();
			
			endereco.setEstado(request.getParameter("estado"));
			endereco.setCidade(request.getParameter("cidade"));
			endereco.setCep(request.getParameter("cep"));
			endereco.setBairro(request.getParameter("bairro"));
			endereco.setRua(request.getParameter("rua"));
			String num = request.getParameter("numero");
			Integer n = Integer.parseInt(num);
			endereco.setNumero(n);
			
			itemPedidoDAO = new ItemPedidoDAO();
			enderecoDao = new EnderecoDAO();
			pedidoDao = new PedidoDAO();
			
			int id = enderecoDao.cadastrar(endereco);
			pedido.setIdEndereco(id);
			pedido.setValor(ip1.getValor()+ip2.getValor()+ip3.getValor());
			int id2 = pedidoDao.cadastrar(pedido);
			
			for(ItemPedido item : listaIP){
				item.setIdPedido(id2);
				itemPedidoDAO.cadastrar(item);
			}
			
			/*
			 * SELECT id FROM tabela ORDER BY id desc LIMIT 1
			 * 
			 */
			
			
			response.getWriter().print("<script> window.alert('Salvo com Sucesso!'); location.href='PedidoController?acao=listar'</script>");

		}else if(acao.equals("Listar")){
			doGet(request, response);
			//response.getWriter().print("<script>location.href='PedidoController?acao=listar'</script>");
		}
		
	}


}