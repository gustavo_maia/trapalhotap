package br.com.larimaia.controller;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import br.com.larimaia.dao.ClienteDAO;
import br.com.larimaia.dao.ProdutoDAO;
import br.com.larimaia.model.Cliente;
import br.com.larimaia.model.Produto;

@WebServlet({ "/ProdutoServlet", "/ProdutoController",
"/ProdutoController.do" })
public class ProdutoController extends HttpServlet {

	private ProdutoDAO dao = new ProdutoDAO();
	private Produto produto;
	
	private static final long serialVersionUID = 1L;
	private static String INSERT_OR_EDIT = "/CadProdutoView.jsp";
    private static String LIST_USER = "/ListaProdutos.jsp";
	
    protected void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		
		String forward="";
		String action = req.getParameter("acao");
		if (action.equalsIgnoreCase("delete")){
			ProdutoDAO dao = new ProdutoDAO();
			String userId = req.getParameter("userId");
			dao.excluir(Integer.parseInt(userId));
            resp.getWriter().print("<script> window.alert('Excluido com Sucesso!');</script>");
            forward = LIST_USER;   
        }else if(action.equalsIgnoreCase("listar")){
			forward = LIST_USER;
		}else if(action.equalsIgnoreCase("cadastrar")){
			forward = INSERT_OR_EDIT;
		}else if(action.equalsIgnoreCase("edit")){
			forward = INSERT_OR_EDIT;
			String id = req.getParameter("userId");
			Produto produtoBuscado = dao.buscarPorId(Integer
					.parseInt(id));

			req.setAttribute("pro", produtoBuscado);
		}
		
		// Levar para o JSP
		RequestDispatcher view = req.getRequestDispatcher(forward);
		view.forward(req, resp);
		
	}
	
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		response.setContentType("text/html");
		
		String acao = request.getParameter("acao");
		
		if(acao.equals("Salvar")){
			produto = new Produto();
			
			String desc = request.getParameter("desc");
			produto.setDescricao(desc);
			
			String valor = request.getParameter("valor");
			produto.setValor(Double.parseDouble(valor));
			
			String id = request.getParameter("id");
			if (id != null && id != "" && id!="0") {
				produto.setIdProduto(Integer.parseInt(id));
			}
			
			ProdutoDAO dao = new ProdutoDAO();
			dao.salvar(produto);
			
			response.getWriter().print("<script> window.alert('Salvo com Sucesso!'); location.href='ProdutoController?acao=listar'</script>");
			
		}else if(acao.equals("Listar")){
			doGet(request, response);
		}
		
	}
	
	
}
