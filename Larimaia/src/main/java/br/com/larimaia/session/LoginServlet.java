package br.com.larimaia.session;
import java.io.IOException;
import java.io.PrintWriter;
 




import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import br.com.larimaia.dao.LoginDAO;
import br.com.larimaia.model.Login;
 
/**
 * Servlet implementation class LoginServlet
 */
@WebServlet("/LoginServlet")
public class LoginServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;
//    private final String userID = "admin";
//    private final String password = "password";
 
    protected void doPost(HttpServletRequest request,
            HttpServletResponse response) throws ServletException, IOException {
    	
    	String botao = request.getParameter("acao");
    	
    	if(botao.equals("Login")){
    	
        	// get request parameters for userID and password
            String user = request.getParameter("user");
            String pwd = request.getParameter("pwd");
            
        	LoginDAO dao = new LoginDAO();
        	Login l = dao.getLogin(user);
        	
        	if(l != null){
        		if(pwd.equals(l.getSenha())){
        			HttpSession session = request.getSession();
        			session.setAttribute("user", user);
        			//setting session to expiry in 30 mins
        			session.setMaxInactiveInterval(30*60);
        			Cookie userName = new Cookie("user", user);
        			userName.setMaxAge(30*60);
        			response.addCookie(userName);
        			response.sendRedirect("Index.jsp");
        		}else{
        			RequestDispatcher rd = getServletContext().getRequestDispatcher("/Login.html");
        			PrintWriter out= response.getWriter();
        			out.println("<font color=red>Senha inv�lida!.</font>");
        			rd.include(request, response);
        		}
        	}else{
        		RequestDispatcher rd = getServletContext().getRequestDispatcher("/Login.html");
        		PrintWriter out= response.getWriter();
        		out.println("<font color=red>Usu�rio n�o cadastrado!.</font>");
        		rd.include(request, response);
        	}
        
    	}else if(botao.equals("Cadastrar")){
    		String username = request.getParameter("user1");
        	String senha = request.getParameter("pwd1");
        	Login l = new Login();
        	l.setSenha(senha);
        	l.setUsername(username);
        	
        	LoginDAO dao = new LoginDAO();
        	dao.cadastrar(l);
        	
        	RequestDispatcher rd = getServletContext().getRequestDispatcher("/Login.html");
			PrintWriter out= response.getWriter();
			out.println("<font color=green>Usu�rio Cadastrado com sucesso!.</font>");
			rd.include(request, response);
    	}
 
    }
    
    protected void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
       
    }
 
}