package br.com.larimaia.model;

import java.util.Date;
import java.util.List;

import br.com.larimaia.dao.ClienteDAO;
import br.com.larimaia.dao.EnderecoDAO;
import br.com.larimaia.dao.TipoEventoDAO;

public class Pedido {
	
	Integer idPedido;
    Cliente cliente;
    Endereco endereco;
    List<ItemPedido> listaItemPedido;
    TipoEvento tipoEvento;
    String origemPedido, indicacao, cerimonial, observacao;
    Date dataPedido, dataHoraEvento;
    Double valor;
    
	public Integer getIdPedido() {
		return idPedido;
	}
	public void setIdPedido(Integer idPedido) {
		this.idPedido = idPedido;
	}
	public Cliente getCliente() {
		return cliente;
	}
	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}
	public Endereco getEndereco() {
		return endereco;
	}
	public void setEndereco(Endereco endereco) {
		this.endereco = endereco;
	}
	public List<ItemPedido> getListaItemPedido() {
		return listaItemPedido;
	}
	public void setListaItemPedido(List<ItemPedido> listaItemPedido) {
		this.listaItemPedido = listaItemPedido;
	}
	public TipoEvento getTipoEvento() {
		return tipoEvento;
	}
	public void setTipoEvento(TipoEvento tipoEvento) {
		this.tipoEvento = tipoEvento;
	}
	public String getOrigemPedido() {
		return origemPedido;
	}
	public void setOrigemPedido(String origemPedido) {
		this.origemPedido = origemPedido;
	}
	public String getIndicacao() {
		return indicacao;
	}
	public void setIndicacao(String indicacao) {
		this.indicacao = indicacao;
	}
	public String getCerimonial() {
		return cerimonial;
	}
	public void setCerimonial(String cerimonial) {
		this.cerimonial = cerimonial;
	}
	public String getObservacao() {
		return observacao;
	}
	public void setObservacao(String observacao) {
		this.observacao = observacao;
	}
	public Date getDataPedido() {
		return dataPedido;
	}
	public void setDataPedido(Date dataPedido) {
		this.dataPedido = dataPedido;
	}
	public Date getDataHoraEvento() {
		return dataHoraEvento;
	}
	public void setDataHoraEvento(Date dataHoraEvento) {
		this.dataHoraEvento = dataHoraEvento;
	}
	public Double getValor() {
		return valor;
	}
	public void setValor(Double valor) {
		this.valor = valor;
	}
    
	public void setIdCliente(Integer id){
        ClienteDAO bo = new ClienteDAO();
        this.setCliente(bo.buscarPorId(id));
    }
    
    public void setIdEndereco(Integer id){
        EnderecoDAO bo = new EnderecoDAO();
        this.setEndereco(bo.buscarPorId(id));
    }
    
    public void setIdTipoEvento(Integer id){
        TipoEventoDAO bo = new TipoEventoDAO();
        this.setTipoEvento(bo.buscarPorId(id));
    }
    
}
