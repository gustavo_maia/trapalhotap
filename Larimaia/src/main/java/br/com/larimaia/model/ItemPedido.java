package br.com.larimaia.model;

import br.com.larimaia.dao.PedidoDAO;
import br.com.larimaia.dao.ProdutoDAO;

public class ItemPedido {

	Integer idItemPedido, quantidade;
    Produto produto;
    Pedido pedido;
    Double valor;
	
    public Integer getIdItemPedido() {
		return idItemPedido;
	}
	public void setIdItemPedido(Integer idItemPedido) {
		this.idItemPedido = idItemPedido;
	}
	public Integer getQuantidade() {
		return quantidade;
	}
	public void setQuantidade(Integer quantidade) {
		this.quantidade = quantidade;
	}
	public Produto getProduto() {
		return produto;
	}
	public void setProduto(Produto produto) {
		this.produto = produto;
	}
	public Pedido getPedido() {
		return pedido;
	}
	public void setPedido(Pedido pedido) {
		this.pedido = pedido;
	}
	public Double getValor() {
		return valor;
	}
	public void setValor(Double valor) {
		this.valor = valor;
	}
    
	public void setIdPedido(Integer id){
        PedidoDAO bo = new PedidoDAO();
        this.setPedido(bo.buscarPorId(id));
    }
    
    public void setIdProduto(Integer id){
        ProdutoDAO bo = new ProdutoDAO();
        this.setProduto(bo.buscarPorId(id));
    }
	
}
