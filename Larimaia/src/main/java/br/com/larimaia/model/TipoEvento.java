package br.com.larimaia.model;

public class TipoEvento {

	Integer idTipoEvento;
    String descricao;
	public Integer getIdTipoEvento() {
		return idTipoEvento;
	}
	public void setIdTipoEvento(Integer idTipoEvento) {
		this.idTipoEvento = idTipoEvento;
	}
	public String getDescricao() {
		return descricao;
	}
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	
	@Override
    public String toString() {
        return this.getDescricao();
    }
    
}
