package br.com.larimaia.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import br.com.larimaia.model.Login;
import br.com.larimaia.util.ConexaoUtil;

public class LoginDAO {
	Connection conexao;
    
    public LoginDAO() {
        conexao = ConexaoUtil.getConnection();
    }
    
    public List<Login> buscarTodos() {
        String sql = "select * from login order by id";
        try {
            PreparedStatement preparadorSQL = conexao.prepareStatement(sql);
            //Armazenando Resultado da consulta
            ResultSet resultado = preparadorSQL.executeQuery();
            List<Login> lista = new ArrayList<>();
            while (resultado.next()) {
                //Instancia de cliente
                Login login = new Login();

                //Atribuindo dados do resultado no objeto cliente
                login.setId(resultado.getInt("id"));
                login.setUsername(resultado.getString("username"));
                login.setSenha(resultado.getString("senha"));
                //Adicionando cliente na lista
                lista.add(login);
            }
            
            preparadorSQL.close();
            return lista;
        } catch (SQLException ex) {

            Logger.getLogger(LoginDAO.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }

    }
    
    public void cadastrar(Login login) {
        String sql = "insert  into login (username,senha) values (?,?)";
        try {
            PreparedStatement preparadorSQL = conexao.prepareStatement(sql);
            preparadorSQL.setString(1, login.getUsername());
            preparadorSQL.setString(2, login.getSenha());
            preparadorSQL.execute();
            preparadorSQL.close();
        } catch (SQLException ex) {
            Logger.getLogger(LoginDAO.class.getName()).log(Level.SEVERE, null, ex);
        }

    }
    
    public Login getLogin(String user){
    	String sql = "select * from login l where l.username =?";
    	
		try {
			PreparedStatement preparadorSQL = conexao.prepareStatement(sql);
			preparadorSQL.setString(1, user);
			//Armazenando Resultado da consulta
			ResultSet resultado = preparadorSQL.executeQuery();
			if (resultado.next()) {
                //Instancia de cliente
                Login login = new Login();

                //Atribuindo dados do resultado no objeto cliente
                login.setId(resultado.getInt("id"));
                login.setSenha(resultado.getString("senha"));
                login.setUsername(resultado.getString("username"));
                preparadorSQL.close();
                return login;
            } else {
                return null;
            }
		} catch (SQLException e) {
			Logger.getLogger(LoginDAO.class.getName()).log(Level.SEVERE, null, e);
            return null;
		}
    }
}
