package br.com.larimaia.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import br.com.larimaia.model.Pedido;
import br.com.larimaia.util.ConexaoUtil;

public class PedidoDAO {
    Connection conexao;
    
    public PedidoDAO() {
        conexao = ConexaoUtil.getConnection();
    }
    
    public Pedido buscarPorId(Integer id) {
        String sql = "select * from pedido where idPedido=?";
        try {
            PreparedStatement preparadorSQL = conexao.prepareStatement(sql);
            preparadorSQL.setInt(1, id);
            //Armazenando Resultado da consulta
            ResultSet resultado = preparadorSQL.executeQuery();
            if (resultado.next()) {
                //Instancia de cliente
                Pedido ped = new Pedido();
                
                
                //Atribuindo dados do resultado no objeto pedido
                ped.setIdPedido(id);
                ped.setCerimonial(resultado.getString("cerimonial"));
                ped.setIdCliente(resultado.getInt("idcliente"));
                ped.setIdEndereco(resultado.getInt("idendereco"));
                ped.setValor(resultado.getDouble("valor"));
                
                Date date = resultado.getDate("dataHoraEvento");
                ped.setDataHoraEvento(date);
                
                Date date2 = resultado.getDate("dataPedido");
                ped.setDataPedido(date2);
                
                ped.setIndicacao(resultado.getString("indicacao"));
                ped.setObservacao(resultado.getString("obs"));
                ped.setOrigemPedido(resultado.getString("origemPedido"));
                ped.setIdTipoEvento(resultado.getInt("idtipoevento"));
                preparadorSQL.close();
                return ped;
            } else {
                return null;
            }
        } catch (SQLException ex) {

            Logger.getLogger(PedidoDAO.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }

    }

    public List<Pedido> getLista() {
        String sql = "select * from pedido order by idPedido";
        try {
            PreparedStatement preparadorSQL = conexao.prepareStatement(sql);
            //Armazenando Resultado da consulta
            ResultSet resultado = preparadorSQL.executeQuery();
            List<Pedido> lista = new ArrayList<>();
            while (resultado.next()) {
                //Instancia de pedido
                Pedido ped = new Pedido();

                //Atribuindo dados do resultado no objeto pedido
                ped.setIdPedido(resultado.getInt("idPedido"));
                ped.setCerimonial(resultado.getString("cerimonial"));
                ped.setIdEndereco(resultado.getInt("idendereco"));
                ped.setIdCliente(resultado.getInt("idcliente"));
                ped.setValor(resultado.getDouble("valor"));
                
                Date date = resultado.getDate("dataHoraEvento");
                SimpleDateFormat dt1 = new SimpleDateFormat("dd-MM-yyyy");
                dt1.format(date);
                ped.setDataHoraEvento(date);
                
                Date date2 = resultado.getDate("dataPedido");
                SimpleDateFormat dt2 = new SimpleDateFormat("dd-MM-yyyy");
                dt2.format(date2);
                ped.setDataPedido(date2);
                
                ped.setIndicacao(resultado.getString("indicacao"));
                ped.setObservacao(resultado.getString("obs"));
                ped.setOrigemPedido(resultado.getString("origemPedido"));
                ped.setIdTipoEvento(resultado.getInt("idtipoevento"));
                //Adicionando produto na lista
                lista.add(ped);
            }
            
            preparadorSQL.close();
            return lista;
        } catch (SQLException ex) {

            Logger.getLogger(PedidoDAO.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }

    }

    public List<Pedido> getPorCliente(Integer idCli){
    	String sql = "select * from pedido p where p.idcliente =?";
        try {
            PreparedStatement preparadorSQL = conexao.prepareStatement(sql);
            preparadorSQL.setInt(1, idCli);
            //Armazenando Resultado da consulta
            ResultSet resultado = preparadorSQL.executeQuery();
            List<Pedido> lista = new ArrayList<>();
            while (resultado.next()) {
                //Instancia de pedido
                Pedido ped = new Pedido();

                //Atribuindo dados do resultado no objeto pedido
                ped.setIdPedido(resultado.getInt("idPedido"));
                ped.setCerimonial(resultado.getString("cerimonial"));
                ped.setIdEndereco(resultado.getInt("idendereco"));
                ped.setIdCliente(resultado.getInt("idcliente"));
                ped.setValor(resultado.getDouble("valor"));
                
                Date date = resultado.getDate("dataHoraEvento");
                ped.setDataHoraEvento(date);
                
                Date date2 = resultado.getDate("dataPedido");
                ped.setDataPedido(date2);
                
                ped.setIndicacao(resultado.getString("indicacao"));
                ped.setObservacao(resultado.getString("obs"));
                ped.setOrigemPedido(resultado.getString("origemPedido"));
                ped.setIdTipoEvento(resultado.getInt("idtipoevento"));
                //Adicionando produto na lista
                lista.add(ped);
            }
            
            preparadorSQL.close();
            return lista;
        } catch (SQLException ex) {

            Logger.getLogger(PedidoDAO.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
    }
    
    public List<Pedido> getPorContrato(java.sql.Date inicial, java.sql.Date fim){
    	String sql = "select * from pedido p where p.datapedido >= ? and p.datapedido <= ?";
        try {
            PreparedStatement preparadorSQL = conexao.prepareStatement(sql);
            preparadorSQL.setDate(1, inicial);
            preparadorSQL.setDate(2, fim);
            //Armazenando Resultado da consulta
            ResultSet resultado = preparadorSQL.executeQuery();
            List<Pedido> lista = new ArrayList<>();
            while (resultado.next()) {
                //Instancia de pedido
                Pedido ped = new Pedido();

                //Atribuindo dados do resultado no objeto pedido
                ped.setIdPedido(resultado.getInt("idPedido"));
                ped.setCerimonial(resultado.getString("cerimonial"));
                ped.setIdEndereco(resultado.getInt("idendereco"));
                ped.setIdCliente(resultado.getInt("idcliente"));
                ped.setValor(resultado.getDouble("valor"));
                
                Date date = resultado.getDate("dataHoraEvento");
                ped.setDataHoraEvento(date);
                
                Date date2 = resultado.getDate("dataPedido");
                ped.setDataPedido(date2);
                
                ped.setIndicacao(resultado.getString("indicacao"));
                ped.setObservacao(resultado.getString("obs"));
                ped.setOrigemPedido(resultado.getString("origemPedido"));
                ped.setIdTipoEvento(resultado.getInt("idtipoevento"));
                //Adicionando produto na lista
                lista.add(ped);
            }
            
            preparadorSQL.close();
            return lista;
        } catch (SQLException ex) {

            Logger.getLogger(PedidoDAO.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
    }
    
    public List<Pedido> getPorDataEvento(java.sql.Date inicial, java.sql.Date fim){
    	String sql = "select * from pedido p where p.datahoraevento >= ? and p.datahoraevento <= ?";
        try {
            PreparedStatement preparadorSQL = conexao.prepareStatement(sql);
            preparadorSQL.setDate(1, inicial);
            preparadorSQL.setDate(2, fim);
            //Armazenando Resultado da consulta
            ResultSet resultado = preparadorSQL.executeQuery();
            List<Pedido> lista = new ArrayList<>();
            while (resultado.next()) {
                //Instancia de pedido
                Pedido ped = new Pedido();

                //Atribuindo dados do resultado no objeto pedido
                ped.setIdPedido(resultado.getInt("idPedido"));
                ped.setCerimonial(resultado.getString("cerimonial"));
                ped.setIdEndereco(resultado.getInt("idendereco"));
                ped.setIdCliente(resultado.getInt("idcliente"));
                ped.setValor(resultado.getDouble("valor"));
                
                Date date = resultado.getDate("dataHoraEvento");
                ped.setDataHoraEvento(date);
                
                Date date2 = resultado.getDate("dataPedido");
                ped.setDataPedido(date2);
                
                ped.setIndicacao(resultado.getString("indicacao"));
                ped.setObservacao(resultado.getString("obs"));
                ped.setOrigemPedido(resultado.getString("origemPedido"));
                ped.setIdTipoEvento(resultado.getInt("idtipoevento"));
                //Adicionando produto na lista
                lista.add(ped);
            }
            
            preparadorSQL.close();
            return lista;
        } catch (SQLException ex) {

            Logger.getLogger(PedidoDAO.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
    }
    
    public Integer salvar(Pedido pedido) {
        if (pedido.getIdPedido()== 0) {
            int i;
            i = cadastrar(pedido);
            return i;
        } else {
            alterar(pedido);
            return 0;
        }
    }

    public Integer cadastrar(Pedido pedido) {
        String sql = "insert  into pedido (origempedido, datapedido, indicacao, datahoraevento, cerimonial, idcliente, obs, idtipoevento, idendereco, valor) values (?,?,?,?,?,?,?,?,?,?)";
        try {
            PreparedStatement preparadorSQL = conexao.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
            preparadorSQL.setString(1, pedido.getOrigemPedido());
            
            Date date = pedido.getDataPedido();
            preparadorSQL.setDate(2, new java.sql.Date(date.getTime()));
            
            preparadorSQL.setString(3, pedido.getIndicacao());
            
            Date date2 = pedido.getDataHoraEvento();
            preparadorSQL.setObject(4, new java.sql.Date(date2.getTime()));
            
            preparadorSQL.setString(5, pedido.getCerimonial());
            preparadorSQL.setInt(6, pedido.getCliente().getIdCliente());
            preparadorSQL.setString(7, pedido.getObservacao());
            preparadorSQL.setInt(8, pedido.getTipoEvento().getIdTipoEvento());
            preparadorSQL.setInt(9, pedido.getEndereco().getIdEndereco());
            preparadorSQL.setDouble(10, pedido.getValor());
            preparadorSQL.execute();
            ResultSet rs = preparadorSQL.getGeneratedKeys();
            int key=0;
            while (rs.next()) {
                key = rs.getInt(1);
            }
            preparadorSQL.close();
            return key;
        } catch (SQLException ex) {
            Logger.getLogger(PedidoDAO.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }

    }

    public void alterar(Pedido pedido) {
        String sql = "update pedido set origempedido=?, datapedido=?, indicacao=?, datahoraevento=?, cerimonial=?, idcliente=?, obs=?, idtipoevento=?, valor=? where idPedido=?";
        try {
            PreparedStatement preparadorSQL = conexao.prepareStatement(sql);
            preparadorSQL.setString(1, pedido.getOrigemPedido());
            
            Date date = pedido.getDataPedido();
            preparadorSQL.setDate(2, new java.sql.Date(date.getTime()));
            
            preparadorSQL.setString(3, pedido.getIndicacao());
            
            Date date2 = pedido.getDataHoraEvento();
            preparadorSQL.setObject(4, new java.sql.Date(date2.getTime()));
            
            preparadorSQL.setString(5, pedido.getCerimonial());
            preparadorSQL.setObject(6, pedido.getCliente().getIdCliente());
            preparadorSQL.setString(7, pedido.getObservacao());
            preparadorSQL.setInt(8, pedido.getTipoEvento().getIdTipoEvento());
            preparadorSQL.setDouble(9, pedido.getValor());
            preparadorSQL.execute();
            preparadorSQL.close();
        }
        catch (SQLException ex) {
            Logger.getLogger(ProdutoDAO.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    public void excluir(Integer id) {
        String sql = "delete from pedido where idPedido=?";

        try {
            PreparedStatement preparadorSQL = conexao.prepareStatement(sql);
            preparadorSQL.setInt(1, id);

            preparadorSQL.execute();
            preparadorSQL.close();
        } catch (SQLException ex) {
            Logger.getLogger(PedidoDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}